# -*- coding: utf-8 -*-
# file: test.py
# author: JinTian
# time: 30/10/2017 3:16 PM
# Copyright 2017 JinTian. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ------------------------------------------------------------------------
import cv2


def test():
    img_file = './data/test_images/timg.jpeg'
    print(img_file)
    img = cv2.imread(img_file)
    cv2.imshow('image', img)
    cv2.waitKey(0)


if __name__ == '__main__':
    test()
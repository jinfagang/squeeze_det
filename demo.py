# Author: Bichen Wu (bichen@berkeley.edu) 08/25/2016

"""SqueezeDet Demo.

In image detection mode, for a given image, detect objects and draw bounding
boxes around them. In video detection mode, perform real-time detection on the
video stream.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2
import time
import sys
import os
import glob

import numpy as np
import tensorflow as tf

from config.kitti_squeezeDet_config import kitti_squeezeDet_config
from config.kitti_squeezeDetPlus_config import kitti_squeezeDetPlus_config
from train import draw_box
from nets import *


FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string(
    'mode', 'image', """'image' or 'video'.""")
tf.app.flags.DEFINE_string(
    'checkpoint', './data/model_checkpoints/squeezeDet/model.ckpt-87000',
    """Path to the model parameter file.""")
tf.app.flags.DEFINE_string(
    'input_path', './data/test_images/*.jpeg',
    """Input image or video to be detected. Can process glob input such as """
    """./data/00000*.png.""")
tf.app.flags.DEFINE_string(
    'out_dir', './data/out/', """Directory to dump output image or video.""")
tf.app.flags.DEFINE_string(
    'demo_net', 'squeezeDet', """Neural net architecture.""")


def video_demo():
    """Detect videos."""

    video_f = './data/video/road_demo.mp4'
    cap = cv2.VideoCapture(video_f)
    cap.set(3, 384)
    cap.set(4, 1248)

    fourcc = cv2.VideoWriter_fourcc(*'XVID')

    out_file_name = os.path.join(os.path.dirname(video_f), os.path.basename(video_f).split('.')[0] + '.avi')
    print('# output video will be saved into: ', out_file_name)
    out = cv2.VideoWriter(out_file_name, fourcc, 20.0, (375, 1242))
    # out.open()

    assert FLAGS.demo_net == 'squeezeDet' or FLAGS.demo_net == 'squeezeDet+', \
        'Selected nueral net architecture not supported: {}'.format(FLAGS.demo_net)

    with tf.Graph().as_default():
        # Load model
        if FLAGS.demo_net == 'squeezeDet':
            mc = kitti_squeezeDet_config()
            mc.BATCH_SIZE = 1
            # model parameters will be restored from checkpoint
            mc.LOAD_PRETRAINED_MODEL = False
            model = SqueezeDet(mc, FLAGS.gpu)
        elif FLAGS.demo_net == 'squeezeDet+':
            mc = kitti_squeezeDetPlus_config()
            mc.BATCH_SIZE = 1
            mc.LOAD_PRETRAINED_MODEL = False
            model = SqueezeDetPlus(mc, FLAGS.gpu)

        saver = tf.train.Saver(model.model_params)

        with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess:
            saver.restore(sess, FLAGS.checkpoint)

            times = {}
            count = 0
            while cap.isOpened():
                t_start = time.time()
                count += 1
                out_im_name = os.path.join(FLAGS.out_dir, str(count).zfill(6) + '.jpg')

                # Load images from video and crop
                ret, frame = cap.read()
                if ret:
                    # crop frames
                    print('frame shape: ', frame.shape)
                    frame = tf.image.resize_image_with_crop_or_pad(frame, mc.IMAGE_HEIGHT, mc.IMAGE_WIDTH).eval()
                    # frame = frame[500:-205, 239:-439, :]
                    im_input = frame.astype(np.float32) - mc.BGR_MEANS
                else:
                    break

                t_reshape = time.time()
                times['reshape'] = t_reshape - t_start

                # Detect
                det_boxes, det_probs, det_class = sess.run(
                    [model.det_boxes, model.det_probs, model.det_class],
                    feed_dict={model.image_input: [im_input]})

                t_detect = time.time()
                times['detect'] = t_detect - t_reshape

                # Filter
                final_boxes, final_probs, final_class = model.filter_prediction(
                    det_boxes[0], det_probs[0], det_class[0])

                keep_idx = [idx for idx in range(len(final_probs)) if final_probs[idx] > mc.PLOT_PROB_THRESH]
                final_boxes = [final_boxes[idx] for idx in keep_idx]
                final_probs = [final_probs[idx] for idx in keep_idx]
                final_class = [final_class[idx] for idx in keep_idx]

                t_filter = time.time()
                times['filter'] = t_filter - t_detect

                # Draw boxes
                cls2clr = {
                    'car': (255, 191, 0),
                    'cyclist': (0, 191, 255),
                    'pedestrian': (255, 0, 191)
                }
                draw_box(
                    frame, final_boxes,
                    [mc.CLASS_NAMES[idx] + ': (%.2f)' % prob for idx, prob in zip(final_class, final_probs)],
                    cdict=cls2clr
                )

                t_draw = time.time()
                times['draw'] = t_draw - t_filter

                # cv2.imwrite(out_im_name, frame)
                out.write(frame)

                times['total'] = time.time() - t_start

                # time_str = ''
                # for t in times:
                #   time_str += '{} time: {:.4f} '.format(t[0], t[1])
                # time_str += '\n'
                time_str = 'Total time: {:.4f}, detection time: {:.4f}, filter time: ' \
                           '{:.4f}'. \
                    format(times['total'], times['detect'], times['filter'])

                print(time_str)

                cv2.imshow('camera', frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
    # Release everything if job is finished
    cap.release()
    # out.release()
    cv2.destroyAllWindows()


def image_demo():
    """Detect image."""

    assert FLAGS.demo_net == 'squeezeDet' or FLAGS.demo_net == 'squeezeDet+', \
        'Selected nueral net architecture not supported: {}'.format(FLAGS.demo_net)

    with tf.Graph().as_default():
        # Load model
        if FLAGS.demo_net == 'squeezeDet':
            mc = kitti_squeezeDet_config()
            mc.BATCH_SIZE = 1
            # model parameters will be restored from checkpoint
            mc.LOAD_PRETRAINED_MODEL = False
            model = SqueezeDet(mc, FLAGS.gpu)
        elif FLAGS.demo_net == 'squeezeDet+':
            mc = kitti_squeezeDetPlus_config()
            mc.BATCH_SIZE = 1
            mc.LOAD_PRETRAINED_MODEL = False
            model = SqueezeDetPlus(mc, FLAGS.gpu)

        saver = tf.train.Saver(model.model_params)

        with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess:
            saver.restore(sess, FLAGS.checkpoint)

            for f in glob.iglob(FLAGS.input_path):
                print('predicting from image: ', f)
                original_im = cv2.imread(f)

                im = tf.image.resize_image_with_crop_or_pad(original_im, mc.IMAGE_HEIGHT, mc.IMAGE_WIDTH).eval()
                # im = cv2.resize(original_im, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT), cv2.INTER_AREA)
                input_image = im - mc.BGR_MEANS

                # Detect
                det_boxes, det_probs, det_class = sess.run(
                    [model.det_boxes, model.det_probs, model.det_class],
                    feed_dict={model.image_input: [input_image]})

                # Filter
                final_boxes, final_probs, final_class = model.filter_prediction(
                    det_boxes[0], det_probs[0], det_class[0])

                keep_idx = [idx for idx in range(len(final_probs)) if final_probs[idx] > mc.PLOT_PROB_THRESH]
                final_boxes = [final_boxes[idx] for idx in keep_idx]
                final_probs = [final_probs[idx] for idx in keep_idx]
                final_class = [final_class[idx] for idx in keep_idx]

                cls2clr = {
                    'car': (255, 191, 0),
                    'cyclist': (0, 191, 255),
                    'pedestrian': (255, 0, 191)
                }

                # Draw boxes
                draw_box(
                    im, final_boxes,
                    [mc.CLASS_NAMES[idx] + ': (%.2f)' % prob for idx, prob in zip(final_class, final_probs)],
                    cdict=cls2clr,
                )

                file_name = os.path.basename(f).split('.')[0]
                out_file_name = os.path.join(os.path.dirname(f), file_name + '.png')
                cv2.imwrite(out_file_name, im)
                print('Image detection output saved to {}'.format(out_file_name))
                cv2.imshow('image', im)
                cv2.waitKey(0)


def main(argv=None):
    if not tf.gfile.Exists(FLAGS.out_dir):
        tf.gfile.MakeDirs(FLAGS.out_dir)
    if FLAGS.mode == 'image':
        image_demo()
    else:
        video_demo()


if __name__ == '__main__':
    tf.app.run()
